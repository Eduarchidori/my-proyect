#include <iostream>
#include <vector>
#include <fstream>
#include <iomanip>
using namespace std;

struct locales{
	 string local;
	 string direc;
	 string dist;
 };
struct promo{
	string linea;
};
struct tipob{
	int nino;
	int adulto;
};
struct cartelera{
	string titulo;
	string formato;
	string idioma;
};
struct peliculas1{
	string titu;
	string titulo;
	string genero;
	string duracion; //en minutos
	string censura;
	string pais;
	string sinopsis;
	string horario1;
	string horario2;
	string horario3;
};
struct peliculas{
	string titu;
	string titulo;
	string genero;
	string duracion; //en minutos
	string censura;
	string pais;
	string sinopsis;
};
struct proximos{
	string titulo;
	string fecha;
	string genero;
	string censura;
	string pais;
};
struct pelicula{
	string titulo;
	string formato;
	string idioma;
	string hora1;
	string hora2;
	string hora3;
};
struct cine {
	string nombre;
	string direc;
	string distrito;
	string telefono;
};
struct usuario{
	string nombre;
	string apellido;
	string sexo;
	string fnaci;
	string email;
	string pass;
};
void mostrar(vector <struct promo> p){
 	for(int i=0; i< p.size();i++){
 		cout << "> " << p[i].linea << endl;
 	}
 }
void promo3(){
	cout<<endl;
	cout<< "*******************FUNCION DE DESCUENTOS********************"<< endl;
	vector <struct promo> p;
	struct promo tmp;
	string narch ="promociones3.txt";
 	ifstream arch(narch.c_str());
 	string line;
	if (arch.is_open()){
		while (!arch.eof()){
 			char separador;
		 	separador='\n';
		 	getline(arch,line,separador);
		 	tmp.linea=line;
		 	p.push_back(tmp);
		}
 	}
	arch.close();
	mostrar(p);
}
void promo2(){
	cout<<endl;
	cout<< "*******************FUNCION DE DESCUENTOS********************"<< endl;
	vector <struct promo> p;
	struct promo tmp;
	string narch ="promociones2.txt";
 	ifstream arch(narch.c_str());
 	string line;
	if (arch.is_open()){
		while (!arch.eof()){
 			char separador;
		 	separador='\n';
		 	getline(arch,line,separador);
		 	tmp.linea=line;
		 	p.push_back(tmp);
		}
 	}
	arch.close();
	mostrar(p);
}
void promo1(){
	cout<<endl;
	cout<< "*******************PROMOCION UNIVERSITARIA********************"<< endl;
	vector <struct promo> p;
	struct promo tmp;
	string narch ="promociones1.txt";
	ifstream arch(narch.c_str());
	string line;
	if (arch.is_open()){
	 	while (!arch.eof()){
	 	 	char separador;
	 		separador='\n';
	 		getline(arch,line,separador);
	 		tmp.linea=line;
	 		p.push_back(tmp);
	 	}
 	}
	arch.close();
	mostrar(p);
}
void muestra(vector <struct pelicula> p){
	for(int i=0; i< p.size();i++){
		cout << "************************" << endl;
		cout <<"Opcion "<< i+1<< endl;
		cout << "Titulo: " << p[i].titulo << endl;
		cout << "Formato: " << p[i].formato << endl;
		cout << "Idioma: " << p[i].idioma << endl;
		cout << "Horario 1: " << p[i].hora1 << endl;
		cout << "Horario 2: " << p[i].hora2 << endl;
		cout << "Horario 3: " << p[i].hora3 << endl;
	}
}
void Prom(){
	int coman;
		cout<<"*************************PROMOCIONES****************************"<<endl;
		cout<<"1. PROMOCION UNIVERSITARIA"<<endl;
		cout<<"2. FUNCION DE DESCUENTOS"<<endl;
		cout<<"3. PROMOCION CUMPLEANIOS"<<endl;
		cout<<"Elegir que opcion quiere mostrar: ";
		cin >> coman;
		switch (coman){
			case 1:
				promo1();
		 	 	break;
		 	case 2:
		 		promo2();
	 	 		break;
		 	case 3:
		 	 	promo3();
		 	 	break;
		 }
}
void comprar(int comando2,struct pelicula q){
	float precio_pelicula;
	int tecla;
	int productos[15];
	for(int i=0;i<15;i++){
		productos[i]=0;
	}
	cout<<endl;
	cout<<"<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< REGISTRO DE COMPRAS >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>"<<endl<<endl;
	int n;
		float x,b=10.20,a=15.60;
		struct tipob bol;
		cout<<"TIPO DE BOLETOS:"<< endl;
		cout<<"1.- Adulto: "<<fixed << setprecision(2)<<a<< endl;
		cout<<"2.- Ninio: "<<fixed << setprecision(2)<< b<< endl;
		cout<<endl;
		cout<<"Cantidad de Adultos: ";
		cin>>bol.adulto;
		cout<<"Cantidad de Ninios: ";
		cin>>bol.nino;
		n=bol.adulto+bol.nino;
		x=bol.adulto*a+bol.nino*b;
		cout<<endl;
		cout<<"Total de la compra es: ";
		cout<<"S/"<<fixed<<setprecision(2)<<(x);
		cout<<endl<<endl;
		cout<<"                SALA 3"<<endl<<endl;
		char asientos[9][20];
		for(int i=0;i<9;i++){
			for(int j=0;j<20;j++){
				asientos[i][j]='O';
			}
		}
		for(int j=0;j<9;j++){
			cout<<j+1<< " ";
			for(int i=0;i<20;i++){
				cout<< asientos[j][i]<<" ";
			}
			cout<< endl<< endl;
		}
		cout<< "  A B C D E F G H I J K L M N O P Q R S T"<< endl;
		cout<< " |---------------------------------------|"<< endl;
		cout<< " |                PANTALLA               |"<< endl;
		cout<< " |---------------------------------------|"<< endl<<endl;
		cout<<"ELEGIR " <<n<<" ASIENTOS:"<<endl<<endl;

		string comando[n];
		for(int k=0;k<n;k++){
			cout<< "Asiento numero "<<k+1<< ": ";
			cin>> comando[k];
		}
		cout<< endl<<endl;
		cout<<"                SALA 3"<<endl<<endl;
		for(int k=0;k<n;k++){

		int a;
		a=(comando[k][0])-49;
		int b;
		switch(comando[k][1]){
			case'A':
				b=0;
				break;
			case'B':
				b=1;
				break;
			case'C':
				b=2;
				break;
			case'D':
				b=3;
				break;
			case'E':
				b=4;
				break;
			case'F':
				b=5;
				break;
			case'G':
				b=6;
				break;
			case'H':
				b=7;
				break;
			case'I':
				b=8;
				break;
			case'J':
				b=9;
				break;
			case'K':
				b=10;
				break;
			case'L':
				b=11;
				break;
			case'M':
				b=12;
				break;
			case'N':
				b=13;
				break;
			case'O':
				b=14;
				break;
			case'P':
				b=15;
				break;
			case'Q':
				b=16;
				break;
			case'R':
				b=17;
				break;
			case'S':
				b=18;
				break;
			case'T':
				b=19;
				break;

		}

			for(int j=0;j<9;j++){
			for(int i=0;i<20;i++){
				if(a==j&&b==i){
					asientos[j][i]='X';
				}
				}
			}
		}
	for(int j=0;j<9;j++){
			cout<<j+1<< " ";
			for(int i=0;i<20;i++){
				cout<< asientos[j][i]<<" ";
			}
			cout<< endl<< endl;
		}
			cout<< "  A B C D E F G H I J K L M N O P Q R S T"<< endl;
			cout<< " |---------------------------------------|"<< endl;
			cout<< " |                PANTALLA               |"<< endl;
			cout<< " |---------------------------------------|"<< endl<<endl;
		float total=0;
			int cont=0;
			bool seguir=1;
			while(seguir==1){
			cout<<endl;
			cout<<"*********************  DULCERIA *********************"<< endl<<endl;
			cout<<"1.- Combos"<< endl;
			cout<<"2.- Cancha"<< endl;
			cout<<"3.- Bebidas"<<endl;
			cout<<"4.- Otros"<<endl;
			cout<<"5.- Seguir al pago"<< endl;
			cout<<endl;
			cout<<"Que opcion desea elegir: ";
			int comando;
			int comando1;
			int opcion;
			cin>>comando;
			switch(comando){
				case 1:
					cout<<endl;
					cout<<"                              COMBOS"<<endl;
					cout<<"----------------------------------------------------------------------"<<endl;
					cout<<"Combo 1     S/25.00        Cancha Mediana, Bebida Mediana"<< endl;
					cout<<"Combo 2     S/32.00        Cancha Grande, Bebida Grande"<< endl;
					cout<<"Combo 3     S/45.00        Cancha Master, Bebida Master,Hotdog Grande"<< endl;
					cout<<"----------------------------------------------------------------------"<<endl;
					cout<<endl;
					cout<<"OPCIONES:"<<endl;
					cout<< "1.- Comprar Combos"<< endl;
					cout<< "2.- Regresar"<< endl<<endl;
					cout<<"Elija alguna opcion: ";
					cin>>opcion;
					cout<<endl;
					if(opcion==1){
						cout<<"                      COMPRANDO COMBOS"<<endl;
						cout<<"Elegir el Combo: ";
						cin>> comando1;
						switch(comando1){
							case 1:
								cout<<"Cantidad: ";
								cin>>cont;
								total +=25*cont;
								productos[0]=cont;
								break;
							case 2:
								cout<<"Cantidad: ";
								cin>>cont;
								total +=32*cont;
								productos[1]=cont;
								break;
							case 3:
								cout<<"Cantidad: ";
								cin>>cont;
								total +=45*cont;
								productos[2]=cont;
								break;
						}
					}
					break;
				case 2:
					cout<<endl;
					cout<<"             CANCHAS"<<endl;
					cout<<"--------------------------------"<<endl;
					cout<<"1.- Cancha Pequeno     S/10.00"<< endl;
					cout<<"2.- Cancha Mediana     S/17.00"<< endl;
					cout<<"3.- Cancha Grande      S/24.00"<< endl;
					cout<<"4.- Cancha Master      S/30.00"<< endl;
					cout<<"--------------------------------"<<endl;
					cout<<endl;
					cout<<"OPCIONES:"<<endl;
					cout<< "1.- Comprar Cancha"<< endl;
					cout<< "2.- Regresar"<< endl;
					cout<<endl;
					cout<<"Elija alguna opcion: ";
					cin>>opcion;
					if(opcion==1){
						cout<<endl;
						cout<<"                      COMPRANDO CANCHAS"<<endl;
						cout<<"Elegir la Cancha: ";
						cin>> comando1;
						switch(comando1){
							case 1:
								cout<<"Cantidad: ";
								cin>>cont;
								total +=10*cont;
								productos[3]=cont;
								break;
							case 2:
								cout<<"Cantidad: ";
								cin>>cont;
								total +=17*cont;
								productos[4]=cont;
								break;
							case 3:
								cout<<"Cantidad: ";
								cin>>cont;
								total +=24*cont;
								productos[5]=cont;
								break;
							case 4:
								cout<<"Cantidad: ";
								cin>>cont;
								total +=30*cont;
								productos[6]=cont;
								break;
							}
						}
					break;
				case 3:
					cout<<endl;
					cout<<"             BEBIDAS"<<endl;
					cout<<"--------------------------------"<<endl;
					cout<<"1.- Bebida Pequeno     S/6.00"<< endl;
					cout<<"2.- Bebida Mediana     S/13.00"<< endl;
					cout<<"3.- Bebida Grande      S/15.00"<< endl;
					cout<<"4.- Bebida Master      S/21.00"<< endl;
					cout<<"--------------------------------"<<endl;
					cout<<endl;
					cout<<"OPCIONES:"<<endl;
					cout<< "1.- Comprar Bebida"<< endl;
					cout<< "2.- Regresar"<< endl;
					cout<<endl;
					cout<<"Elija alguna opcion: ";
					cin>>opcion;
					if(opcion==1){
						cout<<endl;
						cout<<"                   COMPRANDO BEBIDAS"<<endl;
						cout<<"Elegir la Bebida: ";
						cin>> comando1;
						switch(comando1){
							case 1:
								cout<<"Cantidad: ";
								cin>>cont;
								total +=6*cont;
								productos[7]=cont;
								break;
							case 2:
								cout<<"Cantidad: "<< endl;
								cin>>cont;
								total +=13*cont;
								productos[8]=cont;
								break;
							case 3:
								cout<<"Cantidad: "<< endl;
								cin>>cont;
								total +=15*cont;
								productos[9]=cont;
								break;
							case 4:
								cout<<"Cantidad: "<< endl;
								cin>>cont;
								total +=21*cont;
								productos[10]=cont;
								break;
							}
						}
					break;
				case 4:
					cout<<endl;
					cout<<"              OTROS"<<endl;
					cout<<"--------------------------------"<<endl;
					cout<<"1.- Sublime Almendra     S/3.00"<< endl;
					cout<<"2.- Hotdog Normal        S/10.00"<< endl;
					cout<<"3.- Hotdog Grande        S/13.00"<< endl;
					cout<<"4.- Churros             S/7.00"<< endl;
					cout<<"--------------------------------"<<endl;
					cout<<endl;
					cout<<"OPCIONES:"<<endl;
					cout<< "1.- Comprar"<< endl;
					cout<< "2.- Regresar"<< endl;
					cout<<endl;
					cout<<"Elija alguna opcion: ";
					cin>>opcion;
					if(opcion==1){
						cout<<endl;
						cout<<"                             COMPRANDO OTROS"<<endl;
						cout<<"Elija lo que desea comprar: ";
						cin>> comando1;
						switch(comando1){
							case 1:
								cout<<"Cantidad: ";
								cin>>cont;
								total +=3*cont;
								productos[11]=cont;
								break;
							case 2:
								cout<<"Cantidad: ";
								cin>>cont;
								total +=10*cont;
								productos[12]=cont;
								break;
							case 3:
								cout<<"Cantidad: ";
								cin>>cont;
								total +=13*cont;
								productos[13]=cont;
								break;
							case 4:
								cout<<"Cantidad: ";
								cin>>cont;
								total +=7*cont;
								productos[14]=cont;
								break;
							}
						}
					break;
				case 5:
					seguir=0;
					cout<< endl;
					cout<<endl;
					cout<<"-------------CINEMASTER--------------"<< endl<< endl;
					cout<<q.titulo<<"       SALA 3      "<<q.hora2<< endl;
					cout<<"BOLETO>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>"<< endl<< endl;
					if(bol.adulto>0){
					cout<<"Adulto-----------------------------x"<<bol.adulto<< endl<< endl;
					}
					if(bol.nino>0){
					cout<<"Nino-------------------------------x"<<bol.nino<< endl<< endl;
					}
					cout<<"DULCES>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>"<< endl<< endl;
					for(int i=0;i<15;i++){
						switch(i){
						case 0:
							if (productos[i]>0){
								cout<<"Combo 1----------------------------x"<<productos[i]<< endl<< endl;
							}
							break;
						case 1:
							if (productos[i]>0){
								cout<<"Combo 2----------------------------x"<<productos[i]<< endl<< endl;
							}
							break;
						case 2:
							if (productos[i]>0){
								cout<<"Combo 3----------------------------x"<<productos[i]<< endl<< endl;
							}
							break;
						case 3:
							if (productos[i]>0){
								cout<<"Cancha Pequeno---------------------x"<<productos[i]<< endl<< endl;
							}
							break;
						case 4:
							if (productos[i]>0){
								cout<<"Cancha Mediana---------------------x"<<productos[i]<< endl<< endl;
							}
							break;
						case 5:
							if (productos[i]>0){
								cout<<"Cancha Grande----------------------x"<<productos[i]<< endl<<endl;
							}
							break;
						case 6:
							if (productos[i]>0){
								cout<<"Cancha Master----------------------x"<<productos[i]<< endl<< endl;
							}
							break;
						case 7:
							if (productos[i]>0){
								cout<<"Bebida Pequena---------------------x"<<productos[i]<< endl<< endl;
							}
							break;
						case 8:
							if (productos[i]>0){
								cout<<"Bebida Mediana---------------------x"<<productos[i]<< endl<< endl;
							}
							break;
						case 9:
							if (productos[i]>0){
								cout<<"Bebida Grande----------------------x"<<productos[i]<< endl<< endl;
							}
							break;
						case 10:
							if (productos[i]>0){
								cout<<"Bebida Master----------------------x"<<productos[i]<< endl<< endl;
							}
							break;
						case 11:
							if (productos[i]>0){
								cout<<"Sublime Almendra-------------------x"<<productos[i]<< endl<< endl;
							}
							break;
						case 12:
							if (productos[i]>0){
								cout<<"Hotdog Normal----------------------x"<<productos[i]<< endl<< endl;
							}
							break;
						case 13:
							if (productos[i]>0){
								cout<<"Hotdog Grande----------------------x"<<productos[i]<< endl<< endl;
							}
							break;
						case 14:
							if (productos[i]>0){
								cout<<"Churros----------------------------x"<<productos[i]<< endl<< endl;
							}
							break;
							}

					}
					cout<<"El pago total es S/"<<fixed<<setprecision(2)<<x+total<< endl;
					cout<<"Gracias por su compra :)"<< endl;
					break;
			}
		}
}


void mostrar(vector<struct cine> cines){
	for(int i=0; i< cines.size();i++){
		cout << "********************************" << endl;
		cout <<"Opcion "<< i+1<< endl;
		cout << "LOCAL: " << cines[i].nombre << endl;
		cout << "Direccion: " << cines[i].direc << endl;
		cout << "Distrito: " << cines[i].distrito << endl;
		cout << "Telefono: " << cines[i].telefono <<  endl;
	}
}
//mio

void mostrar(vector <struct cartelera> a){
	for(int i=0; i< a.size();i++){
		cout << "************************" << endl;
		cout <<"Pelicula "<< i+1<< endl;
		cout << "Titulo: " << a[i].titulo << endl;
		cout << "Formato: " << a[i].formato << endl;
		cout << "Idioma: " << a[i].idioma << endl;
	}
}
void mostrar(vector <struct peliculas1> b){
	for(int i=0; i< b.size();i++){
		cout << "************************************************ "<<b[i].titu<<" ************************************************"<< endl;
		cout << "Titulo Original: " << b[i].titulo << endl;
		cout << "Genero: " << b[i].genero << endl;
		cout << "Duracion: " << b[i].duracion << " min" << endl;
		cout << "Censura: " << b[i].censura <<  endl;
		cout << "Pais: " << b[i].pais << endl;
		cout<< "Sinopsis: "<<b[i].sinopsis<<endl;
		cout<<"**********************************************************************************************************************"<<endl;
		cout<<"HORARIOS:"<<endl;
		cout<<"  Horario 1: "<<b[i].horario1<<endl;
		cout<<"  Horario 2: "<<b[i].horario2<<endl;
		cout<<"  Horario 3: "<<b[i].horario3<<endl;
	}
}
void mostrar(vector <struct peliculas> b){
	for(int i=0; i< b.size();i++){
		cout << "************************************************ "<<b[i].titu<<" ************************************************"<< endl;
		cout << "Titulo Original: " << b[i].titulo << endl;
		cout << "Genero: " << b[i].genero << endl;
		cout << "Duracion: " << b[i].duracion << " min" << endl;
		cout << "Censura: " << b[i].censura <<  endl;
		cout << "Pais: " << b[i].pais << endl;
		cout<< "Sinopsis: "<<b[i].sinopsis<<endl;
	}
}
void mostrar(vector <struct proximos> c){
	for(int i=0; i< c.size();i++){
		cout << "************************" << endl;
		cout <<"Pelicula "<< i+1<< endl;
		cout << "Titulo: " << c[i].titulo << endl;
		cout << "Fecha: " << c[i].fecha << endl;
		cout << "Genero: " << c[i].genero << endl;
		cout << "Censura: " << c[i].censura <<  endl;
		cout << "Pais: " << c[i].pais << endl;
	}
}
void mostrar(vector <struct locales> l){
	for(int i=0; i< l.size();i++){
		cout << "************************" << endl;
		cout<<"Opcion "<<i+1<<":"<<endl;
		cout << "Local: " << l[i].local << endl;
		cout << "Direccion: " << l[i].direc << endl;
		cout << "Distrito: " << l[i].dist << endl;
	}
}
	void locales(int comando){
	 	cout<<"***********LOCALES***********"<<endl;
	 	vector <struct locales> l;
	 	struct locales tmp;
	 	string narch ="Locales.txt";
	 	ifstream arch(narch.c_str());
	 	string line;
	 	if (arch.is_open()){
	 		while (!arch.eof()){
	 		  	for(int i=0 ; i<3;i++){
	 		  		char separador;
	 		  		if (i < 2){
	 		  			separador=',';
	 		  		}else{
	 		  			separador='\n';
	 		  		}
	 		  		getline(arch,line,separador);
	 		  		switch(i){
	 		  			case 0:
	 		  			tmp.local=line;
	 		  			break;
	 		  			case 1:
	 		  			tmp.direc=line;
	 		  			break;
	 		  			case 2:
	 		  			tmp.dist=line;
	 		  			break;
	 		  		}
	 		  	}
	 		  	l.push_back(tmp);
	 		}
	 	}
		arch.close();
	  	mostrar(l);
	 }

void Detalle(int a){
	cout<<endl;
	vector <struct peliculas> peli;
	struct peliculas tmp;
	string narch = "Peliculas.txt";
	ifstream arch(narch.c_str());
	string line;
	if (arch.is_open()){
		for(int i=0;i<a-1;i++){
			getline(arch,line);
		}
		while (!arch.eof()){
			for(int i=0 ; i<7;i++){
				char separador;
				if (i < 6){
					separador=',';
				}
				else{
					separador='\n';
				}
				getline(arch,line,separador);

				switch(i){
					case 0:
					tmp.titu=line;
					break;
					case 1:
					tmp.titulo=line;
					break;
					case 2:
					tmp.genero=line;
					break;
					case 3:
					tmp.duracion=line;
					break;
					case 4:
					tmp.censura=line;
					break;
					case 5:
					tmp.pais =line;
					break;
					case 6:
					tmp.sinopsis=line;
					break;

				}
			}
			peli.push_back(tmp);
			break;
		}
	}
	arch.close();
	mostrar(peli);
	cout<<"1. Ver locales disponibles"<<endl;
	  	int m;
	  	cin>>m;
	  	locales(m);
}

void Detalle1(int a){
	cout<<endl;
	vector <struct peliculas1> peli;
	struct peliculas1 tmp;
	string narch = "Detalle1.txt";
	ifstream arch(narch.c_str());
	string line;
	if (arch.is_open()){
		for(int i=0;i<a-1;i++){
			getline(arch,line);
		}
		while (!arch.eof()){
			for(int i=0 ; i<10;i++){
				char separador;
				if (i < 9){
					separador=',';
				}
				else{
					separador='\n';
				}
				getline(arch,line,separador);

				switch(i){
					case 0:
					tmp.titu=line;
					break;
					case 1:
					tmp.titulo=line;
					break;
					case 2:
					tmp.genero=line;
					break;
					case 3:
					tmp.duracion=line;
					break;
					case 4:
					tmp.censura=line;
					break;
					case 5:
					tmp.pais =line;
					break;
					case 6:
					tmp.horario1=line;
					break;
					case 7:
					tmp.horario2=line;
					break;
					case 8:
					tmp.horario3=line;
					break;
					case 9:
					tmp.sinopsis=line;
					break;

				}
			}
			peli.push_back(tmp);
			break;
		}
	}
	arch.close();
	mostrar(peli);
}




void Cartelera1(){
	vector <struct pelicula> peliculas;
		struct pelicula tmp;
		string narch = "peliculas 1.txt";
		ifstream arch(narch.c_str());
		string line;
		if (arch.is_open()){
			while (!arch.eof()){
				for(int i=0 ; i<6;i++){
					char separador;
					if (i < 5){
						separador=',';
					}else{
						separador='\n';
					}
					getline(arch,line,separador);
					switch(i){
						case 0:
						tmp.titulo=line;
						break;
						case 1:
						tmp.formato=line;
						break;
						case 2:
						tmp.idioma=line;
						break;
						case 3:
						tmp.hora1=line;
						break;
						case 4:
						tmp.hora2=line;
						break;
						case 5:
						tmp.hora3=line;
						break;
					}
				}
				peliculas.push_back(tmp);
			}
		}
		arch.close();
		muestra(peliculas);
			int comando2;
			int a;
			cout<<"************************"<<endl<<endl;
			cout<<"Elegir la Opcion: ";
			cin>>a;
			Detalle1(a);
			cout<<endl;
			cout<<"Elegir el Horario: ";
			cin>>comando2;
			comprar(comando2,tmp);
}


void Proxe(){
	cout<<endl;
	cout<<"<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< PROXIMOS ESTRENOS >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>"<<endl<<endl;
	vector <struct proximos> pp;
	struct proximos tmp;
	string narch = "Proximos.txt";
	ifstream arch(narch.c_str());
	string line;
	if (arch.is_open()){
		while (!arch.eof()){
			for(int i=0 ; i<5;i++){
				char separador;
				if (i < 4){
					separador=',';
				}
				else{
					separador='\n';
				}
				getline(arch,line,separador);
				switch(i){
					case 0:
					tmp.titulo=line;
					break;
					case 1:
					tmp.fecha=line;
					break;
					case 2:
					tmp.genero=line;
					break;
					case 3:
					tmp.censura=line;
					break;
					case 4:
					tmp.pais =line;
					break;

				}
			}
			pp.push_back(tmp);
		}
	}
	arch.close();
	mostrar(pp);
}
void Nuestroc(){
	cout<<endl;
	cout<<"<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< NUESTROS CINES >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>"<<endl<<endl;
	vector <struct cine> cines;
	struct cine tmp;
	string narch ="Nuestros Cines.txt";
	ifstream arch(narch.c_str());
	string line;
	if (arch.is_open()){
		while (!arch.eof()){
			for(int i=0 ; i<4;i++){
				char separador;
				if (i < 3){
					separador=',';
				}else{
					separador='\n';
				}
				getline(arch,line,separador);
				switch(i){
					case 0:
					tmp.nombre=line;
					break;
					case 1:
					tmp.direc=line;
					break;
					case 2:
					tmp.distrito=line;
					break;
					case 3:
					tmp.telefono=line;
					break;
				}
			}
			cines.push_back(tmp);
		}
	}
	arch.close();
	mostrar(cines);
	int comando;
	cout<<"********************************"<<endl<<endl;
	cout<<"Ir al cine de la Opcion: ";
	cin>> comando;
	cout<<endl;
	cout<<"<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< CARTELERA POR CINE >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>"<<endl<<endl;
	switch (comando){
	case 1:
		Cartelera1();
		break;
	case 2:
		Cartelera1();
		break;
	case 3:
		Cartelera1();
		break;
	case 4:
		Cartelera1();
		break;
	case 5:
		Cartelera1();
		break;
	case 6:
		Cartelera1();
		break;
	case 7:
		Cartelera1();
		break;
	}
}
void Cartelera(){
	cout<<endl;
	cout<<"<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< CARTELERA >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>"<<endl<<endl;
	vector <struct cartelera>cartelera;
	struct cartelera tmp;
	string narch = "Cartelera.txt";
	ifstream arch(narch.c_str());
	string line;
	if (arch.is_open()){
		while (!arch.eof()){
			for(int i=0 ; i<3;i++){
				char separador;
				if (i < 2){
					separador=',';
				}
				else{
					separador='\n';
				}
				getline(arch,line,separador);
				switch(i){
					case 0:
					tmp.titulo=line;
					break;
					case 1:
					tmp.formato=line;
					break;
					case 2:
					tmp.idioma=line;
					break;

				}
			}
			cartelera.push_back(tmp);
		}
	}
	arch.close();
	mostrar(cartelera);
	cout << "************************" << endl<<endl;
	  	cout<<"1. Ver detalle de peliculas"<<endl;
	  	cout<<"2. Comprar"<<endl;
	  	cout<<"eliga opcion"<<endl;
	 	int m;
	  	cin>>m;
	  	if (m==1){
	  		int comando;
	  		cout<<"eliga la opcion de peicula para ver detalles: "<<endl;
	  		cin>> comando;
	  		Detalle(comando);
	  	}else{
	  		Nuestroc();
	  	}
	}
void menur(struct usuario z){
	int comando;
	cout<<endl<<endl;
	cout<<"                                                                                         Bienvenido "<< z.nombre<< "!!"<< endl;
	cout<<endl;
	cout<<"*****************-------------------------  BIENVENIDOS A CINEMASTER  -------------------------*****************"<<endl;
	cout<<endl;

	cout<<"Opciones: "<< endl<<endl;
	cout <<"1.- Cartelera"<<endl;
	cout<<"2.- Proximos estreno"<< endl;
	cout<<"3.- Nuestro cines"<< endl;
	cout<<"4.- Promociones"<<endl<<endl;
	cout<<"Elegir que opcion quiere mostrar: ";
	cin >> comando;
	switch (comando){
	case 1:
		Cartelera();
		break;
	case 2:
		Proxe();
		break;
	case 3:
		Nuestroc();
		break;
	case 4:
		Prom();
		break;
	}
}
void Iniciars(){
	vector <struct usuario> users;
		struct usuario tmp;
		struct usuario user;
		string narch="Usuarios.txt";
		ifstream arch(narch.c_str());
		string line;
			if (arch.is_open()){
				while (!arch.eof()){
					for(int i=0 ; i<6;i++){
						char separador;
						if (i < 5){
							separador=',';
						}else{
							separador='\n';
						}
						getline(arch,line,separador);
						switch(i){
							case 0:
							tmp.nombre=line;
							break;
							case 1:
							tmp.apellido=line;
							break;
							case 2:
							tmp.sexo=line;
							break;
							case 3:
							tmp.fnaci=line;
							break;
							case 4:
							tmp.email=line;
							break;
							case 5:
							tmp.pass=line;
							break;
						}
					}
					users.push_back(tmp);
				}
			}
			arch.close();
	string email;
	string pass;
	bool correct=0;
	while(correct==0){
	cout<<endl;
	cout<<"                  INICIAR SESION                   "<<endl;
	cout<<"***************************************************"<<endl;
	cout<< "Correo electronico: ";
	cin >> email;
	cout<< "Contrasenia: ";
	cin >> pass;
	cout<<"***************************************************"<<endl;
	cout<<endl;
	for(int i=0; i< users.size();i++){
		if(users[i].email==email && users[i].pass==pass){
			user.nombre=users[i].nombre;
			user.apellido=users[i].apellido;
			user.sexo=users[i].sexo;
			user.fnaci=users[i].fnaci;
			user.email=users[i].email;
			user.pass=users[i].pass;
			correct=1;
		}
	}
	if (correct==1){
		cout<<"Se inicio correctamente"<<endl<< endl;
	}
	else{
		cout << "La contrasena o el correo esta mal."<< endl;
		cout << "Vuelve a intentar"<< endl;
	}
	}
	menur(user);
}
void Crearc(){
	struct usuario tmp;
	string rpass;
	string a;
	string b;
	cout<<endl;
	cout<<"                CREAR UNA CUENTA NUEVA                "<<endl;
	cout<<"******************************************************"<<endl;
	cout<< "Nombres: ";
	cin>> a;
	tmp.nombre=a;
	getline(cin,a);
	tmp.nombre +=a;
	cout<< "Apellidos: ";
	cin>> b;
	tmp.apellido=b;
	getline(cin,b);
	tmp.apellido +=b;
	cout<< "Sexo: ";
	cin>>tmp.sexo;
	cout<< "Fecha de nacimiento: ";
	cin>> tmp.fnaci;
	cout<< "Correo electronico: ";
	cin>>tmp.email;
	cout<< "Contrasenia: ";
	cin>> tmp.pass;
	cout<< "Verifique su contrasenia: ";
	cin>> rpass;
	while(tmp.pass!=rpass){
		cout<< "���  Contrasenia erronea  !!!"<<endl;
		cout<< "Vuelva a verifique su contrasenia: ";
		cin>> rpass;
	}
	ofstream arch;
	arch.open("Usuarios.txt",ios::app);
	arch << tmp.nombre <<","<<tmp.apellido<<","<<tmp.sexo<<","<<tmp.fnaci<<","<<tmp.email<<","<<tmp.pass<<endl;
	struct usuario user;
	user.nombre=tmp.nombre;
	user.apellido=tmp.apellido;
	user.sexo=tmp.sexo;
	user.fnaci=tmp.fnaci;
	user.email=tmp.email;
	user.pass=tmp.pass;
	arch.close();
	cout<<"     Felicidades, se ha registrado con exito!!!     "<< endl;
	cout<<"******************************************************";
	menur(user);
}
void Regist(){
	int comando;
	cout<<endl;
	cout<< "<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< REGISTRAR >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>"<<endl<<endl;
	cout << "1.- Iniciar Secion"<< endl;
	cout<< "2.- Crear una nueva cuenta"<< endl<<endl;
	cout<<"Que opcion desea elegir: ";
	cin >> comando;
	switch(comando){
	case 1:
		Iniciars();
		break;
	case 2:
		Crearc();
		break;
	}
}

int main()
{
	string a,b,c;
	int comando;
	cout<<"*****************-------------------------  BIENVENIDOS A CINEMASTER  -------------------------*****************"<<endl;
	cout<<endl;
	cout<<"Opciones: "<< endl<<endl;
	cout <<"1.- Cartelera"<<endl;
	cout<<"2.- Proximos estreno"<< endl;
	cout<<"3.- Nuestro cines"<< endl;
	cout<<"4.- Promociones"<<endl;
	cout<< "5.- Registrar" << endl<<endl;
	cout<<"Elegir que opcion quiere mostrar: ";
	cin >> comando;
	switch (comando){
	case 1:
		Cartelera();
		break;
	case 2:
		Proxe();
		break;
	case 3:
		Nuestroc();
		break;
	case 4:
		Prom();
		break;
	case 5:
		Regist();
		break;

	}
return 0;
}
